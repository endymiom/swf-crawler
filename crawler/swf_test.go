package crawler

import (
	"fmt"
	"io/ioutil"
	"testing"
)

// TestGetSwfLinks executes all of the tests described by formatterTests.
func TestGetSwfLinks(t *testing.T) {
	content, err := ioutil.ReadFile("out/test.swf")
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println("do")

	l := GetLinks(content, []string{"swf", "pdf"})
	for _, link := range l {
		fmt.Println(link)
	}

}

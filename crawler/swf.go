package crawler

import (
	"bytes"
	"compress/zlib"
	"fmt"
	"io"
	"io/ioutil"
	"regexp"
	"strings"
)

// DecompressSWF ...
//
// 1) read the 3 first bytes, compare to CWS, if not equal then exit
// 2) read the rest of the header. unless you need version or file length just leave version . length concatenated. we won't need them.
// 3) decompress everything from byte 9 to EOF
// 4) concatenate "FWS", the 5 header bytes and the decompressed stream
func DecompressSWF(content []byte) ([]byte, error) {
	if string(content[:3]) != "CWS" { // already decompressed or other format
		return content, nil
	}

	reader := bytes.NewReader(content[8:])
	zlibReader, err := zlib.NewReader(reader)
	if err != nil {
		return nil, fmt.Errorf("decompressSWF: %s", err)
	}

	buff := bytes.NewBuffer([]byte{})
	buff.WriteString("FWS")
	buff.Write(content[3:8])
	_, err = io.Copy(buff, zlibReader)
	if err != nil {
		return nil, fmt.Errorf("buffer: %s", err)
	}

	return buff.Bytes(), nil
}

// ExtractFile opens a compressed file and saves it to fileout
func ExtractFile(filename, fileout string) error {
	content, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}

	extracted, err := DecompressSWF(content)
	if err != nil {
		return err
	}

	if err := ioutil.WriteFile(fileout, extracted, 0644); err != nil {
		return err
	}
	return nil
}

// GetLinks retrieve paths to other files
// extensions defines the files types to be retrieved ["swf", "pdf", ...]
func GetLinks(content []byte, extensions []string) (filenames []string) {
	re := fmt.Sprintf(`(?i)\0([^\0]+\.(?:%s))\0`, strings.Join(extensions, "|"))
	r := regexp.MustCompile(re)
	matches := r.FindAllSubmatch(content, -1)
	for _, match := range matches {
		if len(match) == 2 {
			filenames = append(filenames, string(match[1]))
		}
	}
	return filenames
}

package crawler

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

func downloadURL(url string) (content []byte, err error) {
	log.Printf("Downloading %s", url)

	resp, err := http.Get(url)
	if err != nil {
		return nil, fmt.Errorf("Http: %s", err)
	}
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Http status(%d)", resp.StatusCode)
	}

	bodyReader := bufio.NewReader(resp.Body)
	buff := bytes.NewBuffer([]byte{})
	_, err = io.Copy(buff, bodyReader)
	if err != nil {
		return nil, fmt.Errorf("Copy: %s", err)
	}

	return buff.Bytes(), nil
}

// Processor ...
type Processor struct {
	urlBase        string
	downloadedUrls map[string]bool
}

// NewProcessor ...
func NewProcessor(url string) *Processor {
	p := Processor{}
	parts := strings.Split(url, "/")
	p.urlBase = strings.Join(parts[:len(parts)-1], "/") + "/"
	p.downloadedUrls = map[string]bool{}
	return &p
}

func (p *Processor) downloaded(url string) bool {
	if _, ok := p.downloadedUrls[url]; ok { // Already downloaded
		return true
	}
	p.downloadedUrls[url] = true
	return false
}

// ProcessURL downloads extract links and process them
func (p *Processor) ProcessURL(url string) {
	if p.downloaded(url) {
		return
	}

	urlParts := strings.SplitN(url, p.urlBase, 2)
	filename := urlParts[1]

	destination := filepath.Join("out", filename)
	_, err := os.Stat(destination)

	var content []byte
	if os.IsNotExist(err) {
		// Download only if not alredy in disk
		content, err = downloadURL(url)
		if err != nil {
			log.Printf("download error: %v %s", err, url)
			return
		}

		directory := filepath.Dir(destination)
		if err := os.MkdirAll(directory, 0755); err != nil {
			log.Fatal(err)
		}

		log.Printf("Writting %s", destination)
		if err := ioutil.WriteFile(destination, content, 0644); err != nil {
			log.Fatal(err)
		}
	} else {
		content, err = ioutil.ReadFile(destination)
		if err != nil {
			log.Printf("readfile error: %v %s", err, destination)
			return
		}
	}

	extracted, err := DecompressSWF(content)
	if err != nil {
		log.Printf("Error (decompressing): %s", destination)
		return
	}

	filenames := GetLinks(extracted, []string{"swf", "pdf"})
	for _, n := range filenames {
		if strings.HasPrefix(n, "http://") || strings.HasPrefix(n, "https://") {
			continue
		}
		p.ProcessURL(p.urlBase + n)
	}
}

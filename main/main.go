package main

import (
	"flag"
	"log"

	"gitlab.com/endymiom/swf-crawler/crawler"
)

func main() {
	var url string
	flag.StringVar(&url, "url", "", "swf url to download")
	flag.Parse()

	if url == "" {
		log.Fatalln("url not provided")
	}

	p := crawler.NewProcessor(url)
	p.ProcessURL(url)
}

# SWF Crawler

Launch with:

```sh
go run main/main.go <URL>
```

Or build a binary:

```sh
go build main/main.go
```
